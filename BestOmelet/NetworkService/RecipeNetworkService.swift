//
//  RecipeNetworkService.swift
//  BestOmelet
//
//  Created by Viktor on 9/3/17.
//  Copyright © 2017 Viktor Martiukhin. All rights reserved.
//

import Foundation
import Alamofire

enum RecipeNetworkError: Error {
    case invalidURL
}

extension RecipeNetworkError: CustomStringConvertible {

    var description: String {

        switch self {
        case .invalidURL:
            return "Invalid URL"
        }
    }
}

extension RecipeNetworkError: LocalizedError {

    public var errorDescription: String? {

        switch self {
        case .invalidURL:
            return NSLocalizedString("Invalid URL", comment: "")
        }
    }
}

class RecipeNetworkService {
    
    static let shared = RecipeNetworkService()
    private init() {}
    
    func search(for text: String, page: Int, completion: @escaping ([Recipe]?, Error?) -> ()) {
        
        guard let url = recipesURL(searchText: text, page: page) else {
            
            completion(nil, RecipeNetworkError.invalidURL)
            
            return
        }
        
        request(url).validate().responseData { response in
            
            switch response.result {
            case .success(let value):
                
                do {
                    
                    let result = try JSONDecoder().decode(RecipeSearchResult.self, from: value)
                    
                    completion(result.results, nil)
                } catch {
                    
                    completion(nil, error)
                }
            case .failure(let error):
                
                completion(nil, error)
            }
        }
    }
    
    private func recipesURL(searchText: String, page: Int) -> URL? {
        
        var query: String
        
        if searchText.isEmpty {
            
            query = "?i=onions,garlic&q=omelet"
        } else {
            
            let encodedSearchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            query = "?q=\(encodedSearchText)&p=\(page)"
        }
        
        return URL(string: "http://www.recipepuppy.com/api/\(query)")
    }
}
