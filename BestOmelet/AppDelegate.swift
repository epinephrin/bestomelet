//
//  AppDelegate.swift
//  BestOmelet
//
//  Created by Viktor on 9/3/17.
//  Copyright © 2017 Viktor Martiukhin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
}
