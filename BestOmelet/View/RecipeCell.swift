//
//  RecipeCell.swift
//  BestOmelet
//
//  Created by Viktor on 9/3/17.
//  Copyright © 2017 Viktor Martiukhin. All rights reserved.
//

import UIKit
import SDWebImage

class RecipeCell: UITableViewCell {

    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    
    var recipe: Recipe? {
        didSet {
            
            guard let recipe = recipe else { return }
            
            titleLabel!.text = recipe.title.trimmingCharacters(in: .whitespacesAndNewlines)
            ingredientsLabel!.text = recipe.ingredients
            
            thumbnailView!.sd_setImage(with: URL(string: recipe.thumbnail), placeholderImage: UIImage(named: "ThumbnailPlaceholder"))
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        thumbnailView.layer.cornerRadius = thumbnailView.bounds.size.width / 2
        thumbnailView.layer.borderWidth = 1
        thumbnailView.layer.borderColor = UIColor(red: 148/255.0, green: 148/255.0, blue: 148/255.0, alpha: 1.0).cgColor
    }
}
