//
//  SearchViewController.swift
//  BestOmelet
//
//  Created by Viktor on 9/3/17.
//  Copyright © 2017 Viktor Martiukhin. All rights reserved.
//

import UIKit
import RealmSwift

class RecipesSearchViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var navigationBar: UINavigationBar!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    
    
    // MARK: - Properties
    
    private var recipes: Results<Recipe>?
    private var recipesNotificationToken: NotificationToken?
    
    private var hasMoreResipes = true
    private var isRecipesLoading = false {
        didSet {
            
            if isRecipesLoading {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                searchBar.isUserInteractionEnabled = false
                tableFooterSpiner.startAnimating()
            } else {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.searchBar.isUserInteractionEnabled = true
                self.searchBar.resignFirstResponder()
                tableFooterSpiner.stopAnimating()
            }
        }
    }
    
    private let pageSize = 10
    private var nextPage = 1
    
    private lazy var tableFooterSpiner: UIActivityIndicatorView = {
        
        let spiner = UIActivityIndicatorView(style: .gray)
        spiner.hidesWhenStopped = true
        spiner.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)
        
        return spiner
    }()
    
    private lazy var tableNoResultLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Nothing found"
        label.isHidden = true
        
        return label
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {

        return .lightContent
    }
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupView()
        performSearch()
    }
    
    deinit {
        
        recipesNotificationToken?.invalidate()
    }
    
    private func setupView() {
        
        navigationBar.shadowImage = UIImage()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        
        searchBar.backgroundImage = UIImage()
        searchBar.autocapitalizationType = .none
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .black
        
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 106
        
        let tableBackgroundView = UIView(frame: tableView.bounds)
        tableBackgroundView.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
        
        tableView.backgroundView = tableBackgroundView
        
        let tableBackgroundImage = UIImageView()
        tableBackgroundImage.image = UIImage(named: "BackgroundDog")
        tableBackgroundImage.sizeToFit()
        
        tableBackgroundView.addSubview(tableBackgroundImage)
        
        tableBackgroundImage.translatesAutoresizingMaskIntoConstraints = false
        tableBackgroundImage.bottomAnchor.constraint(equalTo: tableBackgroundView.bottomAnchor).isActive = true
        tableBackgroundImage.trailingAnchor.constraint(equalTo: tableBackgroundView.trailingAnchor).isActive = true
        
        tableBackgroundView.addSubview(tableNoResultLabel)
        
        tableNoResultLabel.translatesAutoresizingMaskIntoConstraints = false
        tableNoResultLabel.centerXAnchor.constraint(equalTo: tableBackgroundView.centerXAnchor).isActive = true
        tableNoResultLabel.topAnchor.constraint(equalTo: tableBackgroundView.topAnchor, constant: 20).isActive = true
        
        tableView.tableFooterView = tableFooterSpiner
    }
    
    
    // MARK: - Actions
    
    fileprivate func performSearch() {
        
        isRecipesLoading = true
        
        if nextPage == 1 {
        
            recipes = Recipe.search(for: searchBar.text!)
            
            recipesNotificationToken?.invalidate()
            recipesNotificationToken = recipes?.observe { [weak self] change in
                
                guard let tableView = self?.tableView else { return }
                
                switch change {
                case .initial:
                    
                    tableView.reloadData()
                case .update(_, let deletions, let insertions, let modifications):
                    
                    tableView.beginUpdates()
                    tableView.insertRows(at: insertions.map { IndexPath(item: $0, section: 0) }, with: .automatic)
                    tableView.deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                    tableView.reloadRows(at: modifications.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                    tableView.endUpdates()
                    
                case .error(let error):
                    
                    self?.alert(title: "Error", message: "\(error)")
                }
            }
        }
        
        RecipeNetworkService.shared.search(for: searchBar.text!, page: nextPage) { [weak self] recipes, error in
            
            if let error = error {
                
                self?.alert(title: "Error", message: "\(error)")
            }
            
            if let recipes = recipes {
                
                Recipe.insert(recipes: recipes)
                
                DispatchQueue.main.async {
                    
                    self?.hasMoreResipes = recipes.count == self?.pageSize
                    
                    self?.tableNoResultLabel.isHidden = recipes.count > 0 && (self?.recipes?.count ?? 0) > 0
                }
            }
            
            DispatchQueue.main.async {
                
                self?.isRecipesLoading = false
            }
        }
        
        nextPage += 1
    }
}


// MARK: - UITableViewDataSource

extension RecipesSearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recipes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        
        if let recipe = recipes?[indexPath.row] {
            
            cell.recipe = recipe
        }
        
        if hasMoreResipes,
            !isRecipesLoading,
            !searchBar.text!.isEmpty,
            indexPath.row == (pageSize * (nextPage - 1)) - 1 {

            performSearch()
        }
        
        return cell
    }
}


// MARK: - UITableViewDelegate

extension RecipesSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard
            let recipe = recipes?[indexPath.row],
            let url = URL(string: recipe.href)
        else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}


// MARK: - UISearchBarDelegate

extension RecipesSearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        nextPage = 1
        performSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            
            nextPage = 1
            performSearch()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}
