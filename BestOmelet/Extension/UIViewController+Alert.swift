//
//  UIViewController+Alert.swift
//  BestOmelet
//
//  Created by Viktor on 8/2/19.
//  Copyright © 2019 Viktor Martyukhin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func alert(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        
        alertController.addAction(okAction)
        
        DispatchQueue.main.async {
            
            self.present(alertController, animated: true)
        }
    }
}
