//
//  Recipe.swift
//  BestOmelet
//
//  Created by Viktor on 9/3/17.
//  Copyright © 2017 Viktor Martiukhin. All rights reserved.
//

import Foundation
import RealmSwift

struct RecipeSearchResult: Decodable {
    
    var results = [Recipe]()
}

class Recipe: Object, Decodable {
    
    @objc dynamic var title = ""
    @objc dynamic var ingredients = ""
    @objc dynamic var thumbnail = ""
    @objc dynamic var href = ""
    
    static let realm = try! Realm()
    
    override class func primaryKey() -> String {
        
        return "href"
    }
    
    static func getAll() -> Results<Recipe> {
        
        return realm.objects(Recipe.self)
    }
    
    static func search(for text: String) -> Results<Recipe> {
                
        if text.isEmpty {
            
            return getAll().filter("title CONTAINS[c] 'omelet' AND ingredients CONTAINS[c] 'onions' AND ingredients CONTAINS[c] 'garlic'")
        }
        
        return getAll().filter("title CONTAINS[c] %@", text)
    }
    
    static func insert(recipes: [Recipe]) {
        
        try! realm.write {
            
            realm.add(recipes, update: .all)
        }
    }
}
